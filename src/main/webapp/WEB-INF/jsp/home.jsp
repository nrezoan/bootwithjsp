<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
</head>
<body>
    <h1>Welcome to ${itemObjects} </h1>
    <table>
    	<c:set var="count" value="0" scope="page" />
	    <c:forEach items="${oneItem}" var="lines">
	    <c:set var="count" value="${count + 1}" scope="page"/>
	    		 <tr id="${count}live">       
			     <td>${lines.totalPieces}</td>
			     </tr>
		</c:forEach>
    </table> 
</body>
</html>