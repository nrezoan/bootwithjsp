package com.example.demo;

public class DocParserDataModel {

	private String id;

	private String document_id;

	private String remote_id;

	private String file_name;

	private String media_link;

	private String media_link_original;

	private String media_link_data;

	private String page_count;

	private String uploaded_at;

	private String processed_at;

	private String invoice_no;

	private InvoiceDate invoice_date;

	private String supplier;

	private String buyer;

	private String consignee;

	private String mode_of_transport;

	private String final_destination;

	private String terms_of_delivery;

	private String port_of_loading;

	private String country_of_manufacture;

	private String net_weight;

	private String gross_weight;

	private String to_pay;

	private String shipped_to;

	private String lcsc_number;

	private String exp_number;

	private String po_no;

	private Lcttpodate lcttpodate;

	private ExpDate exp_date;

	private String port_of_discharge;

	private String ref4department;

	private String height_width_length;

	private String terms_of_payment;
	
	private String total_pieces;
	
	private String orderidinpl;

	private String carton_quantity;

	private String warehouse_id;

	private String no_of_packages;

	private String description_of_goods;

	private String price;

	private String hs_code;

	public DocParserDataModel(String id, String document_id, String remote_id, String file_name, String media_link,
			String media_link_original, String media_link_data, String page_count, String uploaded_at,
			String processed_at, String invoice_no, InvoiceDate invoice_date, String supplier, String buyer,
			String consignee, String mode_of_transport, String final_destination, String terms_of_delivery,
			String port_of_loading, String country_of_manufacture, String net_weight, String gross_weight,
			String to_pay, String shipped_to, String lcsc_number, String exp_number, String po_no,
			Lcttpodate lcttpodate, ExpDate exp_date, String port_of_discharge, String ref4department,
			String height_width_length, String terms_of_payment, String total_pieces, String orderidinpl,
			String carton_quantity, String warehouse_id, String no_of_packages, String description_of_goods,
			String price, String hs_code) {
		
		this.id = id;
		this.document_id = document_id;
		this.remote_id = remote_id;
		this.file_name = file_name;
		this.media_link = media_link;
		this.media_link_original = media_link_original;
		this.media_link_data = media_link_data;
		this.page_count = page_count;
		this.uploaded_at = uploaded_at;
		this.processed_at = processed_at;
		this.invoice_no = invoice_no;
		this.invoice_date = invoice_date;
		this.supplier = supplier;
		this.buyer = buyer;
		this.consignee = consignee;
		this.mode_of_transport = mode_of_transport;
		this.final_destination = final_destination;
		this.terms_of_delivery = terms_of_delivery;
		this.port_of_loading = port_of_loading;
		this.country_of_manufacture = country_of_manufacture;
		this.net_weight = net_weight;
		this.gross_weight = gross_weight;
		this.to_pay = to_pay;
		this.shipped_to = shipped_to;
		this.lcsc_number = lcsc_number;
		this.exp_number = exp_number;
		this.po_no = po_no;
		this.lcttpodate = lcttpodate;
		this.exp_date = exp_date;
		this.port_of_discharge = port_of_discharge;
		this.ref4department = ref4department;
		this.height_width_length = height_width_length;
		this.terms_of_payment = terms_of_payment;
		this.total_pieces = total_pieces;
		this.orderidinpl = orderidinpl;
		this.carton_quantity = carton_quantity;
		this.warehouse_id = warehouse_id;
		this.no_of_packages = no_of_packages;
		this.description_of_goods = description_of_goods;
		this.price = price;
		this.hs_code = hs_code;
	}

	public DocParserDataModel() {
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDocument_id() {
		return document_id;
	}

	public void setDocument_id(String document_id) {
		this.document_id = document_id;
	}

	public String getRemote_id() {
		return remote_id;
	}

	public void setRemote_id(String remote_id) {
		this.remote_id = remote_id;
	}

	public String getFile_name() {
		return file_name;
	}

	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}

	public String getMedia_link() {
		return media_link;
	}

	public void setMedia_link(String media_link) {
		this.media_link = media_link;
	}

	public String getMedia_link_original() {
		return media_link_original;
	}

	public void setMedia_link_original(String media_link_original) {
		this.media_link_original = media_link_original;
	}

	public String getMedia_link_data() {
		return media_link_data;
	}

	public void setMedia_link_data(String media_link_data) {
		this.media_link_data = media_link_data;
	}

	public String getPage_count() {
		return page_count;
	}

	public void setPage_count(String page_count) {
		this.page_count = page_count;
	}

	public String getUploaded_at() {
		return uploaded_at;
	}

	public void setUploaded_at(String uploaded_at) {
		this.uploaded_at = uploaded_at;
	}

	public String getProcessed_at() {
		return processed_at;
	}

	public void setProcessed_at(String processed_at) {
		this.processed_at = processed_at;
	}

	public String getInvoice_no() {
		return invoice_no;
	}

	public void setInvoice_no(String invoice_no) {
		this.invoice_no = invoice_no;
	}

	public InvoiceDate getInvoice_date() {
		return invoice_date;
	}

	public void setInvoice_date(InvoiceDate invoice_date) {
		this.invoice_date = invoice_date;
	}

	public String getSupplier() {
		return supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getConsignee() {
		return consignee;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public String getMode_of_transport() {
		return mode_of_transport;
	}

	public void setMode_of_transport(String mode_of_transport) {
		this.mode_of_transport = mode_of_transport;
	}

	public String getFinal_destination() {
		return final_destination;
	}

	public void setFinal_destination(String final_destination) {
		this.final_destination = final_destination;
	}

	public String getTerms_of_delivery() {
		return terms_of_delivery;
	}

	public void setTerms_of_delivery(String terms_of_delivery) {
		this.terms_of_delivery = terms_of_delivery;
	}

	public String getPort_of_loading() {
		return port_of_loading;
	}

	public void setPort_of_loading(String port_of_loading) {
		this.port_of_loading = port_of_loading;
	}

	public String getCountry_of_manufacture() {
		return country_of_manufacture;
	}

	public void setCountry_of_manufacture(String country_of_manufacture) {
		this.country_of_manufacture = country_of_manufacture;
	}

	public String getNet_weight() {
		return net_weight;
	}

	public void setNet_weight(String net_weight) {
		this.net_weight = net_weight;
	}

	public String getGross_weight() {
		return gross_weight;
	}

	public void setGross_weight(String gross_weight) {
		this.gross_weight = gross_weight;
	}

	public String getTo_pay() {
		return to_pay;
	}

	public void setTo_pay(String to_pay) {
		this.to_pay = to_pay;
	}

	public String getShipped_to() {
		return shipped_to;
	}

	public void setShipped_to(String shipped_to) {
		this.shipped_to = shipped_to;
	}

	public String getLcsc_number() {
		return lcsc_number;
	}

	public void setLcsc_number(String lcsc_number) {
		this.lcsc_number = lcsc_number;
	}

	public String getExp_number() {
		return exp_number;
	}

	public void setExp_number(String exp_number) {
		this.exp_number = exp_number;
	}

	public String getPo_no() {
		return po_no;
	}

	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}

	public Lcttpodate getLcttpodate() {
		return lcttpodate;
	}

	public void setLcttpodate(Lcttpodate lcttpodate) {
		this.lcttpodate = lcttpodate;
	}

	public ExpDate getExp_date() {
		return exp_date;
	}

	public void setExp_date(ExpDate exp_date) {
		this.exp_date = exp_date;
	}

	public String getPort_of_discharge() {
		return port_of_discharge;
	}

	public void setPort_of_discharge(String port_of_discharge) {
		this.port_of_discharge = port_of_discharge;
	}

	public String getRef4department() {
		return ref4department;
	}

	public void setRef4department(String ref4department) {
		this.ref4department = ref4department;
	}

	public String getHeight_width_length() {
		return height_width_length;
	}

	public void setHeight_width_length(String height_width_length) {
		this.height_width_length = height_width_length;
	}

	public String getTerms_of_payment() {
		return terms_of_payment;
	}

	public void setTerms_of_payment(String terms_of_payment) {
		this.terms_of_payment = terms_of_payment;
	}

	public String getTotal_pieces() {
		return total_pieces;
	}

	public void setTotal_pieces(String total_pieces) {
		this.total_pieces = total_pieces;
	}

	public String getOrderidinpl() {
		return orderidinpl;
	}

	public void setOrderidinpl(String orderidinpl) {
		this.orderidinpl = orderidinpl;
	}

	public String getCarton_quantity() {
		return carton_quantity;
	}

	public void setCarton_quantity(String carton_quantity) {
		this.carton_quantity = carton_quantity;
	}

	public String getWarehouse_id() {
		return warehouse_id;
	}

	public void setWarehouse_id(String warehouse_id) {
		this.warehouse_id = warehouse_id;
	}

	public String getNo_of_packages() {
		return no_of_packages;
	}

	public void setNo_of_packages(String no_of_packages) {
		this.no_of_packages = no_of_packages;
	}

	public String getDescription_of_goods() {
		return description_of_goods;
	}

	public void setDescription_of_goods(String description_of_goods) {
		this.description_of_goods = description_of_goods;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getHs_code() {
		return hs_code;
	}

	public void setHs_code(String hs_code) {
		this.hs_code = hs_code;
	}

	@Override
	public String toString() {
		return "DocParserDataModel [id=" + id + ", document_id=" + document_id + ", remote_id=" + remote_id
				+ ", file_name=" + file_name + ", media_link=" + media_link + ", media_link_original="
				+ media_link_original + ", media_link_data=" + media_link_data + ", page_count=" + page_count
				+ ", uploaded_at=" + uploaded_at + ", processed_at=" + processed_at + ", invoice_no=" + invoice_no
				+ ", invoice_date=" + invoice_date + ", supplier=" + supplier + ", buyer=" + buyer + ", consignee="
				+ consignee + ", mode_of_transport=" + mode_of_transport + ", final_destination=" + final_destination
				+ ", terms_of_delivery=" + terms_of_delivery + ", port_of_loading=" + port_of_loading
				+ ", country_of_manufacture=" + country_of_manufacture + ", net_weight=" + net_weight
				+ ", gross_weight=" + gross_weight + ", to_pay=" + to_pay + ", shipped_to=" + shipped_to
				+ ", lcsc_number=" + lcsc_number + ", exp_number=" + exp_number + ", po_no=" + po_no + ", lcttpodate="
				+ lcttpodate + ", exp_date=" + exp_date + ", port_of_discharge=" + port_of_discharge
				+ ", ref4department=" + ref4department + ", height_width_length=" + height_width_length
				+ ", terms_of_payment=" + terms_of_payment + ", total_pieces=" + total_pieces + ", orderidinpl="
				+ orderidinpl + ", carton_quantity=" + carton_quantity + ", warehouse_id=" + warehouse_id
				+ ", no_of_packages=" + no_of_packages + ", description_of_goods=" + description_of_goods + ", price="
				+ price + ", hs_code=" + hs_code + "]";
	}
	
		
}

class InvoiceDate {
	
	private String formatted;
	
	public InvoiceDate() {
	
	}
	public InvoiceDate(String formatted) {
		this.formatted = formatted;
	}
	public String getFormatted() {
		return formatted;
	}
	public void setFormatted(String formatted) {
		this.formatted = formatted;
	}
	@Override
	public String toString() {
		return "InvoiceDate [formatted=" + formatted + "]";
	}
	
}

class Lcttpodate {
	
	private String formatted;

	public Lcttpodate() {
	}
	public Lcttpodate(String formatted) {
		super();
		this.formatted = formatted;
	}
	public String getFormatted() {
		return formatted;
	}
	public void setFormatted(String formatted) {
		this.formatted = formatted;
	}
	@Override
	public String toString() {
		return "Lcttpodate [formatted=" + formatted + "]";
	}
	
}

class ExpDate {
	
	private String formatted;

	public ExpDate() {
		
	}
	public ExpDate(String formatted) {
		this.formatted = formatted;
	}
	public String getFormatted() {
		return formatted;
	}
	public void setFormatted(String formatted) {
		this.formatted = formatted;
	}
	@Override
	public String toString() {
		return "ExpDate [formatted=" + formatted + "]";
	}
	
}
