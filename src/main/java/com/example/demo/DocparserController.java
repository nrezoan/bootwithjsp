package com.example.demo;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Controller
public class DocparserController {
    @RequestMapping(value = "/uploadFile", method = RequestMethod.POST	)
    public String uploadFile(
            @RequestParam("uploadfile") MultipartFile uploadfile,Model model) {
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();

        System.out.println("Trying to connect with the docparser");
        System.out.println();
        System.out.println();
        String valueFromDocParser="";
            try {
            	
            	String encodedString=convert(uploadfile);
            	String uploadData=docparserFileUpload(encodedString);
            	Thread.sleep(50000);
            	valueFromDocParser=docparserFileRetrieve(uploadData);
            	System.out.println(valueFromDocParser);
            	LineItemObjectModel[] itemObjects=fetchDocument(valueFromDocParser);
            	//for testing purpose
            	LineItemObjectModel oneItem=itemObjects[0];
            	model.addAttribute("oneItem",Arrays.asList(itemObjects));
            } catch (Exception e) {
            	//here we will have to return the view with null value. so that the fields will
            	//not gonna be populated and won't show any error also
                e.printStackTrace();
            }
            
        return "home";
    }


    //Creating File object of Multipart File
    public String convert(MultipartFile file) throws Exception {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        byte[] bytes = loadFile(convFile);
        byte[] encoded = Base64.encodeBase64(bytes);
        String encodedString = new String(encoded);
        if (convFile.delete())
            System.out.println("Delete file");
        return encodedString;
//        return convFile;
    }



    //Base64 converter
    private static byte[] loadFile(File file) throws IOException {
        InputStream is = new FileInputStream(file);

        long length = file.length();
        if (length > Integer.MAX_VALUE) {
            // File is too large
        }
        byte[] bytes = new byte[(int)length];

        int offset = 0;
        int numRead = 0;
        while (offset < bytes.length
                && (numRead=is.read(bytes, offset, bytes.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < bytes.length) {
        	is.close();
            throw new IOException("Could not completely read file "+file.getName());
        }

        is.close();
        return bytes;
    }




    //File upload method
    private static String docparserFileUpload(String file) {
        String url = "https://api.docparser.com/v1/document/upload/wfrcgctvbdhn";
        // String url="https://api.docparser.com/v1/parsers";
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setAccept(Collections.singletonList(MediaType.ALL));
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        httpHeaders.set("api_key","9cfd72aca8439ff3f367c74d33731ecc4763d2b6");

        body.set("file_content", file);

        HttpEntity<?> entity=new HttpEntity<Object>(body,httpHeaders);
        ResponseEntity<String> responseEntity=restTemplate.exchange(url, HttpMethod.POST,entity,String.class);

        System.out.println(responseEntity.getBody());

        String documentInfo=responseEntity.getBody();
        System.out.println(documentInfo);
        System.out.println();
        System.out.println("Taking time for pre processing");
        System.out.println();
        System.out.println();
        System.out.println();
        
        return documentInfo;
    }





    //parsing document id json
    private static String fetchDocumentInformation(String documentInfo) throws JSONException {
        JSONObject jsonObject = new JSONObject(documentInfo);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        DocParserGETDocumentInfoModel documentId = gson.fromJson(jsonObject.toString(), DocParserGETDocumentInfoModel.class);
        System.out.println("Document Id "+ documentId.getId());
        return documentId.getId();
    }




    // fetching document starts

    //Retrieve

    //File upload method
    private static String docparserFileRetrieve(String uploadedFileJson) throws JSONException {
        String documentId=fetchDocumentInformation(uploadedFileJson);
        String url = "https://api.docparser.com/v1/results/wfrcgctvbdhn/"+documentId;
        System.out.println(url);
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders httpHeaders = new HttpHeaders();

        httpHeaders.setAccept(Collections.singletonList(MediaType.ALL));
        httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

        //  MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

        //   httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
        httpHeaders.set("api_key","9cfd72aca8439ff3f367c74d33731ecc4763d2b6");

        HttpEntity<?> entity=new HttpEntity<Object>(httpHeaders);
        ResponseEntity<String> responseEntity=restTemplate.exchange(url, HttpMethod.POST,entity,String.class);

        System.out.println();
        System.out.println();
        System.out.println();
        return responseEntity.getBody();
        //      String x=responseEntity.getBody().substring(1,responseEntity.getBody().length()-1);
        //    System.out.println(x);
        //  parseFile(x);

    }
    
    
    
    /*
     * return this  method will take the  returned json and convert it into normal java object
     */
    private static LineItemObjectModel[] fetchDocument(String documentInfo) throws Exception {
    	LineItemObjectModel itemObject[]=null;
    	try {
            //JSONObject jsonObject = new JSONObject(documentInfo);
    	       Gson gson = new GsonBuilder().setPrettyPrinting().create();
    	       List<DocParserDataModel> finalDataList = gson.fromJson(documentInfo, new TypeToken<List<DocParserDataModel>>(){}.getType());        
    	       String[] lineItemCount = finalDataList.get(0).getHeight_width_length().split("[\n]");
    	       itemObject=new LineItemObjectModel[lineItemCount.length];
    	       //one loop will be here which will generate all the line item here 
    	       //for the index 0 grossWeight and TotalPieces will have the direct values 
    	       //for other index grossWeight and TotalPieces will have the value 0
    	       for(int i=0;i<lineItemCount.length;i++) {
    	    	   System.out.println(makingLineItemObject(finalDataList.get(0),i));
    	    	   itemObject[i]=makingLineItemObject(finalDataList.get(0),i);
    	       }
    	       return itemObject;
    	}catch(Exception e) {
    		throw e;
    	}

    }
    
    
    /*
     * return this  method will take the  returned json and convert it into normal java object
     * @docParserData which will have the pojo of whole returned json
     * @index says which number of line element will be executed 
     */
    private static LineItemObjectModel makingLineItemObject(DocParserDataModel docParserDataModel,int index) {
    	LineItemObjectModel itemObject=new LineItemObjectModel();
    	itemObject.setPoNo(docParserDataModel.getPo_no());
    	//only the first line item will have the value of these corresponding 0
    	if(index==0) {
    		itemObject.setTotalGrossWeight(docParserDataModel.getGross_weight());
    		itemObject.setTotalPieces(docParserDataModel.getTotal_pieces());
    	}else {
    		itemObject.setTotalGrossWeight("0");
    		itemObject.setTotalPieces("0");
    	}
    	String[] cartoonQuantity = docParserDataModel.getCarton_quantity().split("[*|\n]");
    	itemObject.setCartonQuantity(cartoonQuantity[index]);
    	String[] heightWeightLengthArray = docParserDataModel.getHeight_width_length().split("[\n]");
    	String[] heightWeightLengthSignle=heightWeightLengthArray[index].split("[*]");
    	itemObject.setCartonLength(heightWeightLengthSignle[0]);
    	itemObject.setCartonWidth(heightWeightLengthSignle[1]);
    	itemObject.setCartonHeight(heightWeightLengthSignle[2]);
    	itemObject.sethSCode(docParserDataModel.getHs_code());
    	return itemObject;
    }
    
}
