package com.example.demo;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DocParserGETDocumentInfoModel {
    @JsonProperty("id")
    String id;
    @JsonProperty("file_size")
    String file_size;
    @JsonProperty("pages")
    String pages;

    @JsonProperty("quota_used")
    String quota_used;

    @JsonProperty("quota_left")
    String quota_left;

    @JsonProperty("quota_refill")
    String quota_refill;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getPages() {
        return pages;
    }

    public void setPages(String pages) {
        this.pages = pages;
    }

    public String getQuota_used() {
        return quota_used;
    }

    public void setQuota_used(String quota_used) {
        this.quota_used = quota_used;
    }

    public String getQuota_left() {
        return quota_left;
    }

    public void setQuota_left(String quota_left) {
        this.quota_left = quota_left;
    }

    public String getQuota_refill() {
        return quota_refill;
    }

    public void setQuota_refill(String quota_refill) {
        this.quota_refill = quota_refill;
    }
}
